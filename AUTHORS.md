 Stork authors and contributors
------------------------------

Primary developers:

- Marcin Siodelski
- Michał Nowikowski
- Tomek Mrugalski
- Matthijs Mekking
- Witold Kręcicki

Primary area of work mentioned in parentheses. The list is in a
roughly chronological order.

We have received the following contributions:

 - Franek Górski
   - 2019-09: Grafana template
