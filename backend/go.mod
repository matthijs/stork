module isc.org/stork

go 1.13

require (
	github.com/go-openapi/errors v0.19.2
	github.com/go-openapi/loads v0.19.3
	github.com/go-openapi/runtime v0.19.6
	github.com/go-openapi/spec v0.19.3
	github.com/go-openapi/strfmt v0.19.3
	github.com/go-openapi/swag v0.19.5
	github.com/go-openapi/validate v0.19.3
	github.com/go-pg/migrations/v7 v7.1.6
	github.com/go-pg/pg/v9 v9.0.0-beta.15
	github.com/jessevdk/go-flags v1.4.0
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20191002192127-34f69633bfdc
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297
)
