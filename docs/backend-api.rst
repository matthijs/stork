.. _backend-api:

*****************
Stork Backend API
*****************

URL: (stork-url)/version-get

Returns:

.. code-block:: json

    {
        "version": "1.2.3"
    }


