import { Component, OnInit } from '@angular/core';

import {PanelModule} from 'primeng/panel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

    handler() {
    }
}
